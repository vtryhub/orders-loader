﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Binance.Net.Interfaces;
using Binance.Net.Objects.Spot.UserStream;
using Bot.OrdersLoader.Binance;
using CryptoExchange.Net.Sockets;

namespace Bot.OrdersLoader.Websockets
{
    public class UserDataListener
    {
        private readonly IBinanceSocketClient _binanceSocketClient;
        private readonly ListenKey _listenKey;

        public UserDataListener(IBinanceSocketClient binanceSocketClient, ListenKey listenKey)
        {
            _binanceSocketClient = binanceSocketClient;
            _listenKey = listenKey;

            _ = StartListening();
        }

        private async Task StartListening()
        {
            var x = await _binanceSocketClient.Spot.SubscribeToUserDataUpdatesAsync(
                _listenKey.Key,
                OnOrderUpdateMessageHandler,
                OnOcoOrderUpdateMessageHandler,
                OnAccountPositionMessageHandler,
                OnAccountBalanceUpdateHandler);
        }

        public async Task Stop()
        {
            await _binanceSocketClient.UnsubscribeAllAsync();
        }

        private void OnOrderUpdateMessageHandler(DataEvent<BinanceStreamOrderUpdate> obj)
        {
            OnOrderUpdateMessage.ForEach(x => x(obj));
        }

        private void OnOcoOrderUpdateMessageHandler(DataEvent<BinanceStreamOrderList> obj)
        {
            OnOcoOrderUpdateMessage.ForEach(x => x(obj));
        }

        private void OnAccountPositionMessageHandler(DataEvent<BinanceStreamPositionsUpdate> obj)
        {
            OnAccountPositionMessage.ForEach(x => x(obj));
        }

        private void OnAccountBalanceUpdateHandler(DataEvent<BinanceStreamBalanceUpdate> obj)
        {
            OnAccountBalanceUpdate.ForEach(x => x(obj));
        }

        public List<Action<DataEvent<BinanceStreamOrderUpdate>>> OnOrderUpdateMessage { get; } = new();
        public List<Action<DataEvent<BinanceStreamOrderList>>> OnOcoOrderUpdateMessage { get; } = new();
        public List<Action<DataEvent<BinanceStreamPositionsUpdate>>> OnAccountPositionMessage { get; } = new();
        public List<Action<DataEvent<BinanceStreamBalanceUpdate>>> OnAccountBalanceUpdate { get; } = new();
    }
}
