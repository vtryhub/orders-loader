﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Binance.Net.Enums;
using Binance.Net.Interfaces.SubClients.Spot;
using Binance.Net.Objects.Spot.SpotData;
using Bot.OrdersLoader.Data;
using Bot.OrdersLoader.Extensions;
using CryptoExchange.Net.Objects;
using Hangfire;
using Hangfire.Console;
using Hangfire.Server;

namespace Bot.OrdersLoader.Hangfire
{
    [AutomaticRetry(Attempts = 0)]
    public class OrdersLoader
    {
        private readonly IBinanceClientSpot _binanceClientSpot;
        private readonly MyOrderRepo _myOrderRepo;

        public OrdersLoader(
            IBinanceClientSpot binanceClientSpot,
            MyOrderRepo myOrderRepo)
        {
            _binanceClientSpot = binanceClientSpot;
            _myOrderRepo = myOrderRepo;
        }

        private readonly List<long> _savedOrderIds = new();

        public async Task DoTheJob(PerformContext? context, string coin, long? orderId)
        {
            _savedOrderIds.Clear();
            context.WriteLine($"{coin}. Last order id: {orderId}");
        
            var fetched = 1000;
            while (fetched == 1000)
            {
                WebCallResult<IEnumerable<BinanceOrder>> orders;
                if (orderId is null)
                {
                    orders = await _binanceClientSpot.Order.GetOrdersAsync(
                        $"{coin}BUSD",
                        startTime: new DateTime(2021, 9, 1, 0, 0, 0, DateTimeKind.Utc),
                        limit: 1000);
                }
                else
                {
                    orders = await _binanceClientSpot.Order.GetOrdersAsync(
                        $"{coin}BUSD",
                        orderId: orderId, 
                        limit: 1000);
                }

                if (orders.Error != null)
                    throw new Exception("error");

                var result = orders.Data.ToList();

                await UpsertOrders(result, coin, context);

                fetched = result.Count;
                orderId = result.Select(x => x.OrderId).Concat(new[] { 0l }).Max();
            
                context.WriteLine($"Last update: {result.FirstOrDefault(x => x.OrderId == orderId)?.UpdateTime?.ToString()}");
            }
            
            context.WriteLine("Saved order ids:");
            context.WriteLine(JsonSerializer.Serialize(_savedOrderIds));
            context.WriteLine("Finished");
        }

        private async Task UpsertOrders(List<BinanceOrder> result, string coin, PerformContext? performContext)
        {
            var orderIds = result.Select(x => x.OrderId).ToList();

            var existingOrders = await _myOrderRepo.Items($"{coin}BUSD1")
                .Where(x => orderIds.Contains(x.OrderId))
                .ToMyListAsync();

            foreach (var binanceOrder in result)
            {
                var existingOrder = existingOrders.FirstOrDefault(x => x.OrderId == binanceOrder.OrderId);

                if (binanceOrder.Status == OrderStatus.Canceled)
                {
                    if (existingOrder != null)
                    {
                        performContext.WriteLine($"Order with id {binanceOrder.OrderId} was cancelled. Deleting the order.");
                        await _myOrderRepo.Delete(existingOrder.OrderIdSymbolUserId, existingOrder.SymbolUserId);
                    }
                }
                else
                {
                    if (existingOrder is null)
                    {
                        performContext.WriteLine($"Order with id {binanceOrder.OrderId} is not found. Adding new order:");
                        performContext.WriteLine(SerializeOrder(binanceOrder));
                        var newOrder = MyOrder.FromBinanceOrder(binanceOrder);
                        await _myOrderRepo.Upsert(newOrder);
                        _savedOrderIds.Add(newOrder.OrderId);
                    }
                    else if (binanceOrder.UpdateTime > existingOrder.UpdateTime)
                    {
                        performContext.WriteLine($"Order with id {binanceOrder.OrderId} has bigger update time " +
                                                 $"{binanceOrder.UpdateTime} vs order (id: {existingOrder.OrderId} " +
                                                 $"update time: {existingOrder.UpdateTime}). Updating with new data:");
                        performContext.WriteLine(SerializeOrder(binanceOrder));
                        var newOrder = MyOrder.FromBinanceOrder(binanceOrder);
                        await _myOrderRepo.Upsert(newOrder);
                        _savedOrderIds.Add(binanceOrder.OrderId);
                    }
                }
            }
        }

        private string SerializeOrder(BinanceOrder o)
        {
            return $"OrderId={o.OrderId}, ClientOrderId={o.ClientOrderId}, CreateTime={o.CreateTime}, UpdateTime={o.UpdateTime}";
        }
    }
}
