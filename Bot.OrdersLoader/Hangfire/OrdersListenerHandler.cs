﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Binance.Net.Enums;
using Binance.Net.Objects.Spot.UserStream;
using Bot.OrdersLoader.Data;
using Bot.OrdersLoader.Extensions;
using Bot.OrdersLoader.Websockets;
using CryptoExchange.Net.Sockets;
using Hangfire;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Bot.OrdersLoader.Hangfire
{
    [AutomaticRetry(Attempts = 0)]
    public class OrdersListenerHandler : IHostedService
    {
        private readonly List<string> _coins;
        private readonly UserDataListener _userDataListener;
        private readonly MyOrderRepo _myOrderRepo;

        private static readonly SemaphoreSlim SemaphoreSlim = new(1, 1);
        
        public OrdersListenerHandler(
            IConfiguration configuration,
            UserDataListener userDataListener,
            MyOrderRepo myOrderRepo)
        {
            _userDataListener = userDataListener;
            _myOrderRepo = myOrderRepo;
            _coins = configuration["Coins"].Split(" ").ToList();
        }
        
        private async void OnOrderUpdateMessage(DataEvent<BinanceStreamOrderUpdate> e)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                var newOrder = MyOrder.FromBinanceOrderUpdate(e.Data);
                var existingOrder = await _myOrderRepo.Items(newOrder.SymbolUserId)
                    .Where(x => x.OrderIdSymbolUserId == newOrder.OrderIdSymbolUserId)
                    .MyFirstOrDefaultAsync();

                if (newOrder.Status == OrderStatus.Canceled)
                {
                    if (existingOrder != null)
                    {
                        await _myOrderRepo.Delete(existingOrder.OrderIdSymbolUserId, existingOrder.SymbolUserId);
                    }
                }
                else
                {
                    if (existingOrder is null || newOrder.UpdateTime > existingOrder.UpdateTime)
                    {
                        await _myOrderRepo.Upsert(newOrder);
                    }
                }
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }
        
        private async void OnOcoOrderUpdateMessage(DataEvent<BinanceStreamOrderList> e)
        {
        }
        
        private async void OnAccountPositionMessage(DataEvent<BinanceStreamPositionsUpdate> e)
        {
        }
        
        private async void OnAccountBalanceUpdate(DataEvent<BinanceStreamBalanceUpdate> e)
        {
        }
        
        
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            RecurringJob.AddOrUpdate<ListenKeyUpdater>(nameof(ListenKeyUpdater), x => x.DoTheJob(default), Cron.MinuteInterval(15));
            
            var latestFinishedOrders = new Dictionary<string, long?>();
            foreach (var coin in _coins)
            {
                latestFinishedOrders[coin] = await _myOrderRepo.Items($"{coin}BUSD1")
                    .Where(x => x.Status != OrderStatus.New && x.Status != OrderStatus.PartiallyFilled)
                    .OrderByDescending(x => x.UpdateTime)
                    .Select(x => x.OrderId)
                    .MyFirstOrDefaultAsync();
                if (latestFinishedOrders[coin] == 0)
                    latestFinishedOrders[coin] = null;
            }
            
            _userDataListener.OnOrderUpdateMessage.Add(OnOrderUpdateMessage);
            _userDataListener.OnOcoOrderUpdateMessage.Add(OnOcoOrderUpdateMessage);
            _userDataListener.OnAccountPositionMessage.Add(OnAccountPositionMessage);
            _userDataListener.OnAccountBalanceUpdate.Add(OnAccountBalanceUpdate);

            foreach (var (coin, orderId) in latestFinishedOrders)
            {
                BackgroundJob.Enqueue<OrdersLoader>(x => x.DoTheJob(default, coin, orderId));
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _userDataListener.Stop();
        }
    }
}
