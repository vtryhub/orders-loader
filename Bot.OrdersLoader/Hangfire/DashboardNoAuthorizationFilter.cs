﻿using Hangfire.Dashboard;

namespace Bot.OrdersLoader.Hangfire
{
    public class DashboardNoAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext dashboardContext)
        {
            return true;
        }
    }
}
