﻿using System.Threading.Tasks;
using Binance.Net.Interfaces.SubClients.Spot;
using Bot.OrdersLoader.Binance;
using Hangfire.Console;
using Hangfire.Server;

namespace Bot.OrdersLoader.Hangfire
{
    public class ListenKeyUpdater
    {
        private readonly ListenKey _listenKey;
        private readonly IBinanceClientSpot _binanceClientSpot;

        public ListenKeyUpdater(ListenKey listenKey, IBinanceClientSpot binanceClientSpot)
        {
            _listenKey = listenKey;
            _binanceClientSpot = binanceClientSpot;
        }

        public async Task DoTheJob(PerformContext? context)
        {
            context.WriteLine("Updating listen key...");
            await _binanceClientSpot.UserStream.KeepAliveUserStreamAsync(_listenKey.Key);
            context.WriteLine("Updating listen updated");
        }
    }
}