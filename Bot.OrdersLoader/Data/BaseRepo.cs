﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;

namespace Bot.OrdersLoader.Data
{
    public class BaseRepo<T>
    {
        private readonly Container _container;

        public BaseRepo(CosmosClient cosmosClient, string container)
        {
            _container = cosmosClient.GetContainer("Bot", container);
        }
        
        public IOrderedQueryable<T> Items(string partitionKey) => _container
            .GetItemLinqQueryable<T>(requestOptions: new QueryRequestOptions { PartitionKey = new PartitionKey(partitionKey) });
        // public IOrderedQueryable<T> Items() => _container
        //     .GetItemLinqQueryable<T>();

        public virtual async Task Add(T item, string partitionKey)
        {
            await _container.CreateItemAsync(item, new PartitionKey(partitionKey));
        }
        
        public virtual async Task Replace(T item, string id, string partitionKey)
        {
            await _container.ReplaceItemAsync(item, id, new PartitionKey(partitionKey));
        }
        
        public virtual async Task Upsert(T item, string partitionKey)
        {
            await _container.UpsertItemAsync(item, new PartitionKey(partitionKey));
        }
        
        public virtual async Task Delete(string id, string partitionKey)
        {
            await _container.DeleteItemAsync<T>(id, new PartitionKey(partitionKey));
        }
    }
}
