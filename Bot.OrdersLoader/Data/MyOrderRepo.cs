using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;

namespace Bot.OrdersLoader.Data
{
    public class MyOrderRepo : BaseRepo<MyOrder>
    {
        public MyOrderRepo(CosmosClient cosmosClient) : base(cosmosClient, "Orders")
        {
        }

        public virtual async Task Add(MyOrder item)
        {
            await base.Add(item, item.SymbolUserId);
        }
        public Task Replace(MyOrder item)
        {
            return base.Replace(item, item.OrderIdSymbolUserId, item.SymbolUserId);
        }
        public Task Upsert(MyOrder item)
        {
            return base.Upsert(item, item.SymbolUserId);
        }
    }
}
