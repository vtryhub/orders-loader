﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Bot.OrdersLoader.Data
{
    public class Poza
    {
        [JsonProperty("id")]
        public string SellOrderIdSymbolUserId { get; set; }
        public string SymbolUserId { get; set; }
        
        public DateTime ClosedAt { get; set; }
        public DateTime OpenedAt { get; set; }
        
        public List<OrderIds> AffectedBuyOrders { get; set; }
        public List<OrderIds> AffectedSellOrders { get; set; }
        
        public decimal BuyQuantity { get; set; }
        public decimal SellQuantity { get; set; }
        
        public decimal SellQuoteQuantity { get; set; }
        public decimal BuyQuoteQuantity { get; set; }

        public decimal BuyAvgPrice { get; set; }
        public decimal SellAvgPrice { get; set; }
        
        public decimal PercentProfit { get; set; }
        
        public decimal QuantityLeft { get; set; }
        public decimal QuoteQuantityLeft { get; set; }

        public string GetSymbol()
        {
            return SymbolUserId.Substring(0, SymbolUserId.IndexOf("BUSD", StringComparison.InvariantCulture));
        }
    }

    public class OrderIds
    {
        public long OrderId { get; set; }
        public string ClientOrderId { get; set; }

        public override int GetHashCode()
        {
            return OrderId.GetHashCode() + ClientOrderId.GetHashCode();
        }

        public override bool Equals(object? obj)
        {
            return obj is OrderIds x && x.GetHashCode() == GetHashCode();
        }
    }
}
