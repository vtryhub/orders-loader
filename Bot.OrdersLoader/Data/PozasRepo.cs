using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;

namespace Bot.OrdersLoader.Data
{
    public class PozasRepo : BaseRepo<Poza>
    {
        public PozasRepo(CosmosClient cosmosClient) : base(cosmosClient, "Pozas")
        {
        }
        
        public virtual async Task Add(Poza item)
        {
            await base.Add(item, item.SymbolUserId);
        }
        public Task Replace(Poza item)
        {
            return base.Replace(item, item.SellOrderIdSymbolUserId, item.SymbolUserId);
        }
        public Task Upsert(Poza item)
        {
            return base.Upsert(item, item.SymbolUserId);
        }
    }
}
