﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Binance.Net.Enums;
using Binance.Net.Objects.Spot.SpotData;
using Microsoft.Azure.Cosmos.Linq;

namespace Bot.OrdersLoader.Extensions
{
    public static class MyExtensions
    {
        public static string ToJson(this object o) =>
            JsonSerializer.Serialize(o,
                new JsonSerializerOptions { WriteIndented = true, Converters = { new JsonStringEnumConverter() } });

        public static string StringJoin(this IEnumerable<string> e, string separator) =>
            string.Join(separator, e);

        public static string ToMyOrderId(this BinanceOrder o) => new StringBuilder(o.OrderId.ToString())
            .Append('_').Append(o.Symbol).Append('_').Append(1).ToString();

        public static string ToPartitionKey(this BinanceOrder o) => new StringBuilder(o.Symbol).Append('1').ToString();

        public static bool IsActive(this BinanceOrder o) =>
            o.Status is OrderStatus.New or OrderStatus.PartiallyFilled;


        public static async Task<List<T>> ToMyListAsync<T>(this IQueryable<T> q)
        {
            using var setIterator = q.ToFeedIterator();
            var result = new List<T>();
            
            while (setIterator.HasMoreResults)
            {
                result.AddRange(await setIterator.ReadNextAsync());
            }

            return result;
        }
        
        public static async Task<T?> MyFirstOrDefaultAsync<T>(this IQueryable<T> q)
        {
            using var setIterator = q.ToFeedIterator();
            var result = default(T);
            
            while (setIterator.HasMoreResults)
            {
                foreach (var item in await setIterator.ReadNextAsync())
                {
                    result = item;
                    break;
                }
                break;
            }

            return result;
        }
    }
}
